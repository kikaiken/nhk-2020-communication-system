import struct
from enum import Enum
from roslibpy import Message, Ros, Topic

class PacketType(Enum):
    RosBodySubscribeRequest = 1
    RosBodySubscribeResponse = 2
    RosBodyAdvertiseRequest = 3
    RosBodyAdvertiseResponse =4
    RosBodyPublish = 5
    RosBodyError = 6
    ComBody = 7
    ComBody = 8



class PacketBase():
    header_length = 4
    header_signature = 0x4B

    @staticmethod
    def parse(message:bytes):
        signature, packet_type = struct.unpack_from('<BB', message)
        if signature != PacketBase.header_signature:
            return None

        if packet_type == PacketType.RosBodySubscribeRequest.value:
            return PacketRosBodySubscribeRequest.parse(message)
        else:
            return None

    @staticmethod
    def header_to_bytes(packet_type, session_id):
        return struct.pack('<BBH', PacketBase.header_signature, packet_type, session_id)

class Micon_Set():
    micon_ip = 
    micon_port = 9092
    portrate = 


class PacketRosBodySubscribeRequest(PacketBase):
    packet_type = PacketType.RosBodySubscribeRequest

    def __init__(self):
        self.session_id = 0
        self.topic_name_length = 0
        self.topic_name = ""

    def __str__(self):
        return "[{}] SessionID:0x{:04X} TopicName:'{}'".format(self.__class__.__name__, self.session_id, self.topic_name)

    @staticmethod
    def parse(message:bytes):
        #ヘッダーからsessionIDを取り出す
        pkt = PacketRosBodySubscribeRequest()
        pkt.session_id = struct.unpack_from('<xxH', message)[0]

        #データ部分の配列を取り出す
        payload = message[PacketBase.header_length:]

        #データをパースする
        #トピック名の長さを取り出す
        pkt.topic_name_length = struct.unpack_from('<B', payload)[0]
        #トピック名を取り出す(NULL終端文字は削除する)
        pkt.topic_name = (payload[1:]).decode('utf-8').replace('\0', '')

        #データ上のトピック名の長さと実際のトピック名の長さが一致することを確認する
        if pkt.topic_name_length != len(pkt.topic_name):
            print("Invalid TopicNameLength:{}".format(pkt.topic_name_length))
            return None
        else:
            return pkt

    def to_bytes(self):
        #str型のtopic_nameをbytes型に変換
        topic_name_bytes = self.topic_name.encode('utf-8')
        #topic_nameのbyte数を取得
        self.topic_name_length = len(topic_name_bytes)

        #ヘッダーをbytes型に変換
        header_bytes = PacketBase.header_to_bytes(PacketRosBodySubscribeRequest.packet_type.value, self.session_id)
        #データ部分をbytes型に変換。bytes型の配列を連結することで可変長strに対応している
        payload_bytes = struct.pack('<B', self.topic_name_length) + topic_name_bytes + b'\0'

        return header_bytes+payload_bytes

class PacketRosBodySubscribeResponse(PacketBase, RosBodySubscribeRequest):
    packet_type = PacketType.RosBodySubscribeResponse
    packet_session_id = self.session_id
    
    def __init__(self):
        self.payload_bytes

    def packing(self): 
        if topic_type_repository.topic_name != None
            status = '0'
        #ヘッダ作成、バイト列化
        hoge = PacketBase()
        header_bytes = hoge.header_to_bytes(packet_type, packet_session_id)
        #payloadの作成、バイト
        self.payload_bytes = status +  struct.pack('<LB', topic_id_repositpry[self.topic_name], topic_format_repository[self.topic_name])

        return header_bytes+payload_bytes

    #マイコンにパケットを送信
    def send(self):
        from socket import socket, AF_INET, SOCK_DGRAM

        with socket(AF_INET, SOCK_DGRAM) as s:
            s.sendto(packing(), ((micon_address, micon_port))


class PacketRosBodyAdvertiseRequest(PacketBase):
    packet_type = PacketType.RosBodyAdvertiseRequest

    def __init__(self):
        self.session_id = 0
        self.topic_name_length = 0
        self.topic_name = ""

    def __str__(self):
        return "[{}] SessionID:0x{:04X} TopicName:'{}'".format(self.__class__.__name__, self.session_id, self.topic_name)

    @staticmethod
    def parse(message:bytes):
        #ヘッダーからsessionIDを取り出す
        pkt = PacketRosBodyAdvertiseRequest()
        pkt.session_id = struct.unpack_from('<xxH', message)[0]

        #データ部分の配列を取り出す
        payload = message[PacketBase.header_length:]

        #データをパースする
        #トピック名の長さを取り出す
        pkt.topic_name_length = struct.unpack_from('<B', payload)[0]
        #トピック名を取り出す(NULL終端文字は削除する)
        pkt.topic_name = (payload[1:]).decode('utf-8').replace('\0', '')

        #データ上のトピック名の長さと実際のトピック名の長さが一致することを確認する
        if pkt.topic_name_length != len(pkt.topic_name):
            print("Invalid TopicNameLength:{}".format(pkt.topic_name_length))
            return None
        else:
            return pkt

    def to_bytes(self):
        #str型のtopic_nameをbytes型に変換
        topic_name_bytes = self.topic_name.encode('utf-8')
        #topic_nameのbyte数を取得
        self.topic_name_length = len(topic_name_bytes)

        #ヘッダーをbytes型に変換
        header_bytes = PacketBase.header_to_bytes(PacketRosBodyAdvertiseRequest.packet_type.value, self.session_id)
        #データ部分をbytes型に変換。bytes型の配列を連結することで可変長strに対応している
        payload_bytes = struct.pack('<B', self.topic_name_length) + topic_name_bytes + b'\0'

        return header_bytes+payload_bytes
    
class PacketRosBodyAdvertiseResponse(PacketBase, RosBodyAdvertiseRequest):
    packet_type = PacketType.RosBodyAdvetiseResponse
    packet_session_id = self.session_id
    
    def __init__(self):
        self.payload_bytes

    def packing(self): 
        if topic_type_repository.topic_name != None
            status = '0'
        #ヘッダ作成、バイト列化
        hoge = PacketBase()
        header_bytes = hoge.header_to_bytes(packet_type, packet_session_id)
        #payloadの作成、バイト
        self.payload_bytes = status +  struct.pack('<LB', topic_id_repositpry[self.topic_name], topic_format_repository[self.topic_name])

        return header_bytes+payload_bytes

    #マイコンにパケットを送信
    def send(self):
        from socket import socket, AF_INET, SOCK_DGRAM

        with socket(AF_INET, SOCK_DGRAM) as s:
            s.sendto(packing(), ((micon_address, micon_port))


class RosTopicPublish(PacketBase,PacketRosBodySubscribeRequest):
    packet_type = PacketType.RosBodyPublish
    topic_type = topic_type_repository.self.topic_name.value
    def pc_to_micon(self):  
        from rosbridge import order,subscribe
        order('subscribe',self.topic_name, topic_type)#name,type未定
        my_sub = subscribe()
        client = roslibpy.Ros(host ='localhost', port = 9090)
        listener = roslibpy.Topic(client, self.topic_name, topic_type)
        listener.subscribe(my_sub.listen)

        try:
            while True:
                pass
        except KeyboardInterrupt:
            client.terminate
 
    def micon_to_pc(self):
        from socket import socket, AF_INET, SOCK_DGRAM
        #マイコンのデータ入力
        host = ''
        port = 9092
        with socket(AF_INET, SOCK_DGRAM) as s:
            s.bind((host, port))
            while True:
                data, addr = s.recvfrom(1024)
                PacketRosBodySubscribeRequest.parse(data)
                #ros形式に変換
                #やり取りするtopicの詳細が来たら続き

if __name__ == "__main__":
    #受信したパケットをパースする
    recv_data = b'\x4B\x01\x05\x00\x01\x48\x00\x00'
    pkt = PacketBase.parse(recv_data)
    print('recv content: {}'.format(pkt))

    #送信するためにパケットを作成する
    pkt = PacketRosBodySubscribeRequest()
    pkt.session_id = 42
    pkt.topic_name = "HelloWorld"
    send_data = pkt.to_bytes()
    print(send_data)

    #パケット作成出来ているか確認
    test_pkt = PacketBase.parse(send_data)
    print('send content: {}'.format(test_pkt))
