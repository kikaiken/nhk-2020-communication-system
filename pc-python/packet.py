import struct
from enum import Enum

class PacketType(Enum):
    RosBodySubscribeRequest = 1
    RosBodySubscribeResponse = 2

class PacketBase():
    header_length = 4
    header_signature = 0x4B

    @staticmethod
    def parse(message:bytes):
        signature, packet_type = struct.unpack_from('<BB', message)
        if signature != PacketBase.header_signature:
            return None

        if packet_type == PacketType.RosBodySubscribeRequest.value:
            return PacketRosBodySubscribeRequest.parse(message)
        else:
            return None

    @staticmethod
    def header_to_bytes(packet_type, session_id):
        return struct.pack('<BBH', PacketBase.header_signature, packet_type, session_id)

class PacketRosBodySubscribeRequest(PacketBase):
    packet_type = PacketType.RosBodySubscribeRequest

    def __init__(self):
        self.session_id = 0
        self.topic_name_length = 0
        self.topic_name = ""

    def __str__(self):
        return "[{}] SessionID:0x{:04X} TopicName:'{}'".format(self.__class__.__name__, self.session_id, self.topic_name)

    @staticmethod
    def parse(message:bytes):
        #ヘッダーからsessionIDを取り出す
        pkt = PacketRosBodySubscribeRequest()
        pkt.session_id = struct.unpack_from('<xxH', message)[0]

        #データ部分の配列を取り出す
        payload = message[PacketBase.header_length:]

        #データをパースする
        #トピック名の長さを取り出す
        pkt.topic_name_length = struct.unpack_from('<B', payload)[0]
        #トピック名を取り出す(NULL終端文字は削除する)
        pkt.topic_name = (payload[1:]).decode('utf-8').replace('\0', '')

        #データ上のトピック名の長さと実際のトピック名の長さが一致することを確認する
        if pkt.topic_name_length != len(pkt.topic_name):
            print("Invalid TopicNameLength:{}".format(pkt.topic_name_length))
            return None
        else:
            return pkt

    def to_bytes(self):
        #str型のtopic_nameをbytes型に変換
        topic_name_bytes = self.topic_name.encode('utf-8')
        #topic_nameのbyte数を取得
        self.topic_name_length = len(topic_name_bytes)

        #ヘッダーをbytes型に変換
        header_bytes = PacketBase.header_to_bytes(PacketRosBodySubscribeRequest.packet_type.value, self.session_id)
        #データ部分をbytes型に変換。bytes型の配列を連結することで可変長strに対応している
        payload_bytes = struct.pack('<B', self.topic_name_length) + topic_name_bytes + b'\0'

        return header_bytes+payload_bytes

class PacketRosBodySubscribeResponse(PacketBase, PacketRosBodySubscribeRequest):
    def __init__(self):
        self.TopicID = 0
        self.topic_name = ""
        self.micon_ip = 0
        self.topic_format = ""
        self.session_id = 0

    def rosbridge():
        self.topic_name = PacketRosBodySubscribeRequest.topic_name
        import receive

if __name__ == "__main__":
    #受信したパケットをパースする
    recv_data = b'\x4B\x01\x05\x00\x01\x48\x00\x00'
    pkt = PacketBase.parse(recv_data)
    print('recv content: {}'.format(pkt))

    #送信するためにパケットを作成する
    pkt = PacketRosBodySubscribeRequest()
    pkt.session_id = 42
    pkt.topic_name = "HelloWorld"
    send_data = pkt.to_bytes()
    print(send_data)

    #パケット作成出来ているか確認
    test_pkt = PacketBase.parse(send_data)
    print('send content: {}'.format(test_pkt))
